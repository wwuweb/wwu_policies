<?php

/**
 * @file
 * WWU Policies helper functions.
 */

/**
 * Get the correct number prefix based on document type.
 */
function wwu_policies_get_prefix($bundle) {
  switch ($bundle) {

    case 'policy':
      $prefix = 'POL';

      break;

    case 'procedure':
      $prefix = 'PRO';

      break;

    case 'standard':
      $prefix = 'STN';

      break;

    case 'form':
      $prefix = 'FRM';

      break;

    case 'task':
      $prefix = 'TSK';

      break;

    default:
      $prefix = '';

  }

  $prefix = (!empty($prefix)) ? $prefix . '-U' : $prefix;

  return $prefix;
}
